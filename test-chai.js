const chai = require('chai');
const expect = chai.expect;

describe('Test chai', ()=> {
    it('should compare thing by expect', ()=> {
        expect(1).to.equal(1);
    });

    it('should compare another things by expect', ()=> {
        expect(5>8).to.be.false;
        expect({name: 'bew'}).to.deep.equal({name: 'bew'});
        expect({name: 'bew'}).to.have.property('name').to.equal('bew');
        expect({}).to.be.a('object');
        expect(1).to.be.a('number');
        expect('bew').to.be.a('string');
        expect('bew'.length).to.equal(3);
        expect('bew').to.lengthOf(3);
        expect([1,2,3]).to.lengthOf(3);
        expect(null).to.be.null;
        expect(undefined).to.not.exist;
        expect(1).to.exist;
    });
});